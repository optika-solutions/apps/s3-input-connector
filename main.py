import boto3
import os
import shutil


def akumen(**kwargs):
    """
    Parameters:
        - Input: bucket_name [string]
        - Input: object_name [string]
        - Input: access_key [string]
        - Input: secret_access_key [string]

        - Output: data [file] (data.csv)
        - Output: data_size [float]
    """
    
    if not kwargs.get('access_key') or not kwargs.get('secret_access_key'):
        raise Exception('Did not specify access_key or secret_access_key.')
    
    if not kwargs.get('bucket_name') or not kwargs.get('object_name'):
        raise Exception('Did not specify bucket or object name.')
    
    session = boto3.Session(
        aws_access_key_id=kwargs.get('access_key'),
        aws_secret_access_key=kwargs.get('secret_access_key')
    )
    
    s3 = session.client('s3')
    
    s3.download_file(kwargs.get('bucket_name'), kwargs.get('object_name'), 'outputs/data.csv')
    
    return {
        'data_size': os.path.getsize('outputs/data.csv')
    }
